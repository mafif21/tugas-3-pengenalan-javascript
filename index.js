//Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// jawaban soal 1
var pertamaBaru = pertama.substring(0, 4) + " " + pertama.substring(12, 18) + " ";
var keduaBaru = kedua.substring(0, 7) + " " + kedua.substring(8, 18).toUpperCase();
console.log(pertamaBaru.concat(keduaBaru));

//soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//jawaban soal 2
var a = parseInt(kataPertama);
var b = parseInt(kataKedua);
var c = parseInt(kataKetiga);
var d = parseInt(kataKeempat);
var total = a + b * c + d;
console.log(total);

//soal 3
var kalimat = "wah javascript itu keren sekali";

//jawaban soal no 3
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 15); // do your own! (menghasilkan Javascript)
var kataKetiga = kalimat.substring(15, 19); // do your own! (menghasilkan itu)
var kataKeempat = kalimat.substring(19, 25); // do your own! (menghasilkan keren)
var kataKelima = kalimat.substring(25, 31); // do your own! (menghasilkan sekali)

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
